**Hola, aqui mi documentacion para los snippets nuevos**

Para reemplazar imagenes al final de la linea la configuracion es esta

```php
<?php
//REEMPLAZAR ENLACES EN UNA SOLA LINEA POR NOMBRE


        $list[] = array("DOMINIO" => "amzn.to",
            "TEXTO" => "La puedes conseguir aqui",
            "IMAGE" => '<img src="http://images.assets-landingi.com/DE9xaEYu20dCbIg9/comprar_paypal.png" alt="" width="350" height="103" srcset="https://images.assets-landingi.com/DE9xaEYu20dCbIg9/comprar_paypal.png 1x, https://images.assets-landingi.com/5edfFf35/comprar_paypal.png 2x">',
            "BUTTON" => '<button    style=" background-color: #000; border: none;  color: white; padding: 15px 32px; text-align: center;  text-decoration: none;  display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer;"  > #LINK# </button>');

 //REEMPLAZAR ENLACES POR NOMBRE
       $list[] = array("DOMINIO" => "amzn.to",
            "TEXTO" => "La puedes conseguir aqui",
            "IMAGE" => '<img src="http://images.assets-landingi.com/DE9xaEYu20dCbIg9/comprar_paypal.png" alt="" width="350" height="103" srcset="https://images.assets-landingi.com/DE9xaEYu20dCbIg9/comprar_paypal.png 1x, https://images.assets-landingi.com/5edfFf35/comprar_paypal.png 2x">',
            "BUTTON" => '<button    style=" background-color: #000; border: none;  color: white; padding: 15px 32px; text-align: center;  text-decoration: none;  display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer;"  > #LINK# </button>');




?>
```

 - **DOMINIO :** dominio sobre el cual se quiere aplicar el cambio, ejemplo amazon.com,amazon,es
 - **TEXTO:** texo del enlace es el texto que se encuentra entre > here < de la etiqueta <a>
 - **IMAGE:** imagen para el botton tal como el ejemplo de arriba
 - **BUTTON:** elemento por el cual se quiere reemplazar el enlace , esto basicamente lo que va a hacer es envolver el link dentro del nuevo elemento, puede ser dentro de un button,span, dev etc...
 
  **NOTA** : Si deseas usar BUTTON en vez de IMAGE debes remover la posicion IMAGE DEL ARRAY O RENOMBRAR 
 
